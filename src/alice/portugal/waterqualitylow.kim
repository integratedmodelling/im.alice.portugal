private namespace alice.portugal.waterqualitylow;

/*LOADS */
@intensive(space)
model local:stefano.balbi:alice.sandbox:bn.ss.load.low.simple.cutchange
		as ratio of (chemistry:Suspension physical:Solid im:Mass) to (im:Area of hydrology:Watershed) in kg    
	observing 
	    im:Area of hydrology:Watershed in km^2                                                                named area_of_watershed, 
		proportion of landcover:BroadleafForest in (im:Area of hydrology:Watershed)                           named broadleaf_forest_proportion_area_of_watershed,
		im:Numerosity of im:Outgoing earth:Stream                                                             named outgoing_stream_count,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed) named riparian_broadleaf_forest_proportion_area_of_watershed;   

@intensive(space)
model local:stefano.balbi:alice.sandbox:bn.no3.load.low.simple.cutchange
		as ratio of (chemistry.incubation:Nitrate im:Mass) to (im:Area of hydrology:Watershed) in kg //kg/trimester (during low flows - summer)
	observing 
	    im:Area of hydrology:Watershed in km^2                                                                named area_of_watershed, 
		proportion of landcover:BroadleafForest in (im:Area of hydrology:Watershed)                           named broadleaf_forest_proportion_area_of_watershed,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed) named riparian_broadleaf_forest_proportion_area_of_watershed;

@intensive(space)
model local:stefano.balbi:alice.sandbox:bn.po4.load.low.simple2 
		as ratio of (chemistry.incubation:Phosphate im:Mass) to (im:Area of hydrology:Watershed) in kg //kg/trimester (during low flows - summer)
	observing 
	    im:Area of hydrology:Watershed in km^2                                                                named area_of_watershed,  
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed) named riparian_broadleaf_forest_proportion_area_of_watershed,
		value of landcover:Urban im:Area                                                                      named urban_proportion_area_of_watershed,
		im:Numerosity of im:Outgoing earth:Stream                                                             named outgoing_stream_count; 
 
//from loads to CONCENTRATIONS    
/*------------------------------------------ */
//NO3_SumC = NO3_SumL / Flow_Sum * 1.26*10^(-4)
//PO4_SumC = PO4_SumL / Flow_Sum * 1.26*10^(-4)  
//SS_SumC  = SS_SumL  / Flow_Sum * 1.26*10^(-7)    

@intensive(space)
model ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L
	observing 
		IUPAC:Water im:Volume during earth:Summer in mm named streamflow,
		ratio of (chemistry.incubation:Nitrate im:Mass) to (im:Area of hydrology:Watershed) in kg named no3_load
	set to [
		def temp = ((streamflow <= 2) ? 2 : ((streamflow > 2) && (streamflow <= 4) ? 4 : 5))
		def result = no3_load / temp * 1.26*10**(-4)
		(result > 1000) ? (result = nodata) : (result)
		return result
		];      

@intensive(space)
model ratio of (chemistry.incubation:Phosphate im:Mass) to (IUPAC:Water im:Volume)  in mg/L 
	observing 
		IUPAC:Water im:Volume during earth:Summer in mm named streamflow,
		ratio of (chemistry.incubation:Phosphate im:Mass) to (im:Area of hydrology:Watershed) in kg named po4_load
	set to [
		def temp = ((streamflow <= 2) ? 2 : ((streamflow > 2) && (streamflow <= 4) ? 4 : 5))
		def result = po4_load / temp * 1.26*10**(-4)
		(result > 100) ? (result = nodata) : (result)
		return result
		];  

//the two above are need to take the output of ml.waterquality and use them in ml.metabolism (no3 also in ml.diversity)
//the following is needed to take the output of ml.waterquality and use it in ml.diversity
     
@intensive(space)
model ratio of (chemistry:Suspension physical:Solid  im:Mass) to (IUPAC:Water im:Volume) in mg/L
	observing 
		IUPAC:Water im:Volume during earth:Summer in mm named streamflow,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (im:Area of hydrology:Watershed) in kg named ss_load
	set to [
		def temp = ((streamflow <= 2) ? 2 : ((streamflow > 2) && (streamflow <= 4) ? 4 : 5))
		def result = ss_load / temp * 1.26*10**(-4)
		(result > 1000) ? (result = nodata) : (result)
		return result
		]; 